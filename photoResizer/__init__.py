"""
Pretty image resizer with image centering and cropping.

:creationdate: 2021-12-13 20:06
:moduleauthor: François GUÉRIN <frague59@gmail.com>
:modulename: photoResizer

"""
__version__ = VERSION = "0.2.3"
__author__ = "François GUÉRIN"
__author_email__ = "frague59@gmail.com"
__license__ = "MIT"
__copyright__ = "Copyright (c) 2021 François GUÉRIN"
__url__ = "https://gitlab.com/frague59/photoResizer"
