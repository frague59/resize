# Image resizer

Resize images with face centering and cropping, export images into a directory.

## Installation

### Without face detection

#### With pip, from sources
```shell
$ python3 -m pip --user install git+https://gitlab.com/frague59/resize.git
```

#### With pip, from PyPi
```shell
$ python3 -m pip --user install photoResizer
```


#### With pipx
```shell
$ pipx install photoResizer
```

### With face detection (opencv)

#### With pip
```shell
$ python3 -m pip --user install photoResizer[opencv] 
```

#### With pipx
```shell
$ pipx install photoResizer[opencv]
```

## Usage 

```shell
$ photoResizer --help
Usage: python -m photoResizer [OPTIONS] IMAGES... OUTPUT_DIR

  Resize images to a given width and height.

Options:
  --version
  -W, --width INTEGER             Width of the output image, default: 600.
  -H, --height INTEGER            Height of the output image, default: 800.
  -Q, --quality INTEGER           Quality of the output image (JPEG output,
                                  default: 90).
  -f, --format [jpeg|png|tiff|webp]
                                  Type of the output image, default: jpeg.
  -p, --multiprocessing           Use multiprocessing for image resizing,
                                  default: False.
  -F, --face-detection            Use face detection for image resizing
                                  (OpenCV required, default: False).
  --help                          Show this message and exit.

```

`IMAGES` can be a list of image paths or a directory. In case of a directory, the pattern `*.[Jj][Pp][Ee]?[Gg]` is applied to the files.
